import { products } from "./api.js";

const fillUpCatalog = async (sort = null) => {
  const productList = await products(sort);
  let productHTML = " ";
  const catalog = document.getElementsByClassName("catalog")[0];
  for (const product of productList) {
    productHTML += `<div class="catalog__product">
    <div class="catalog__image">
    <input type ="radio">
      <img
        src="${product.image}"
      />
    </div>
    <div class="catalog__text">
      <h2>
      ${product.title}
      </h2>
</div>

    <div class="catalog__prices">
        <h3>
        ${product.price}
      </h3>
    </div>
  </div>`;
  }
  catalog.innerHTML = productHTML;
};

fillUpCatalog();

const sort = document.getElementById("sort");
sort.addEventListener("change", () => {
  fillUpCatalog(sort.value);
});

const searchQuery = document.getElementById("searchQuery");
const buttonQuery = document.getElementById("buttonQuery");
buttonQuery.addEventListener("click", () => {
  products(sort).then((result) => {
    const filteredProducts = result.filter(
      (product) => product.title.indexOf(searchQuery.value) != -1
    );
    let productHTML = " ";
    const catalog = document.getElementsByClassName("catalog")[0];
    for (const product of filteredProducts) {
      productHTML += `<div class="catalog__product">
      <div class="catalog__image">
      <input type ="radio">
        <img
          src="${product.image}"
        />
      </div>
      <div class="catalog__text">
        <h2>
        ${product.title}
        </h2>
  </div>
      <div class="catalog__prices">
          <h3>
          ${product.price}
        </h3>
      </div>
    </div>`;
    }
    catalog.innerHTML = productHTML;
  });
});
