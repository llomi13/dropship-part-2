import { SERVER_ADDR } from "./config.js";

const call = async (url) => {
  const req = await fetch(SERVER_ADDR + url);
  return await req.json();
};

export const products = async (sort = null) => {
  return await call(`products${sort ? `?sort=${sort}` : ""}`);
};

export const categories = async () => await call(`products/categories`);
